package hnclient

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type HNItem struct {
	ID    int64
	By    string
	Kids  []int64
	Title string
	Type  string
	Score int64
	Url   string
}

func GetTopStories(n int) ([]HNItem, error) {
	ids, err := getTopStoriesIDs()
	if err != nil {
		return nil, err
	}

	if len(ids) < n {
		n = len(ids)
	}

	result := make([]HNItem, 0, n)
	for _, id := range ids[:n] {
		item, err := getHNItem(id)
		if err != nil {
			return nil, err
		}
		result = append(result, *item)
	}

	return result, nil
}

func getTopStoriesIDs() ([]int64, error) {
	resp, err := http.Get("https://hacker-news.firebaseio.com/v0/topstories.json")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	topItemIds := make([]int64, 100)
	err = json.Unmarshal(body, &topItemIds)
	if err != nil {
		return nil, err
	}

	return topItemIds, nil
}

func getHNItem(id int64) (*HNItem, error) {
	resp, err := http.Get(fmt.Sprintf("https://hacker-news.firebaseio.com/v0/item/%d.json", id))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result HNItem
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
