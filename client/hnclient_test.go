package hnclient

import (
	"fmt"
	"testing"
)

func TestGetTopStories(t *testing.T) {
	items, err := GetTopStories(4)
	if err != nil {
		t.Fatal(err)
	}

	if len(items) <= 0 {
		t.Fatal("Failed to receive top items")
	}

	fmt.Printf("%#v", items)
}

func TestGetTopStoriesIDs(t *testing.T) {
	items, err := getTopStoriesIDs()
	if err != nil {
		t.Fatal(err)
	}

	if len(items) <= 0 {
		t.Fail()
	}
}

func TestGetHNItem(t *testing.T) {
	item, err := getHNItem(18004244)
	if err != nil {
		t.Fatal(err)
	}

	if item.By != "glastra" || item.Title != "Gell-Mann amnesia effect" {
		t.Fail()
	}
}
